import React from 'react';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';

const SimpleApp = React.lazy(() => import('Remote1/SimpleApp'));

function App() {
  return (
    <div className="App">
      <Typography variant="h3" gutterBottom>
        Ejemplo simple MFE
      </Typography>
      <Divider />
      <SimpleApp />
    </div>
  );
}

export default App;
